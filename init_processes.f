      subroutine init_processes
      use openloops_powheg
      implicit none
      include "nlegborn.h"
      include "pwhg_flst.h"
      include "pwhg_st.h"
c      include "coupl.inc"
      include 'pwhg_kn.h'
      include 'LesHouches.h'
      include 'pwhg_res.h'
      integer i
      real * 8 powheginput
      external powheginput

      integer nmaxres_real,nmaxres_born,dim_integ,ismaxres

      lprup(1)=10000 ! 10000

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C    Set here the number of light flavours
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      st_nlight=4 ! b is massive!

      print*,'number of light flavors =',st_nlight

      flst_numfinal=nlegbornexternal-2
      call init_processes_born
      call init_processes_real

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C    Set coupling powers of Born cross section
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      res_powew=0
      res_powst=4

! no resonances in process -> do not build resonance histories
!       if(powheginput("#nores") /= 1) then
!          call build_resonance_histories
!       endif
!
      write(*,*) ' ***********   FINAL POWHEG    ***************'
      write(*,*) ' ***********     BORN          ***************'
      do i=1,flst_nborn
c         write(*,'(a,100i4)')'                  ',int(1:flst_bornlength(i))
         write(*,'(a,i3,a,100i4)')'flst_born(',i,')   =',flst_born(1:flst_bornlength(i),i)
         write(*,'(a,i3,a,100i4)')'flst_bornres(',i,')=',flst_bornres(1:flst_bornlength(i),i)
         write(*,*)
      enddo
      write(*,*) 'max flst_bornlength: ',maxval(flst_bornlength(1:flst_nborn))



      write(*,*) ' ***********     REAL          ***************'
      do i=1,flst_nreal
c         write(*,'(a,100i4)')'                  ',int(1:flst_reallength(i))
         write(*,'(a,i3,a,100i4)')'flst_real(',i,')   =',flst_real(1:flst_reallength(i),i)
         write(*,'(a,i3,a,100i4)')'flst_realres(',i,')=',flst_realres(1:flst_reallength(i),i)
         write(*,*)
      enddo
      write(*,*) 'max flst_reallength: ',maxval(flst_reallength(1:flst_nreal))

      call buildresgroups(flst_nborn,nlegborn,flst_bornlength,
     1     flst_born,flst_bornres,flst_bornresgroup,flst_nbornresgroup)


      call buildresgroups(flst_nreal,nlegreal,flst_reallength,
     1     flst_real,flst_realres,flst_realresgroup,flst_nrealresgroup)

      write(*,*)
      write(*,*) '*********   SUMMARY  *********'
      write(*,*) 'set nlegborn to: ',maxval(flst_bornlength(1:flst_nborn))
      write(*,*) 'set nlegreal to: ',maxval(flst_reallength(1:flst_nreal))

      nmaxres_real = maxval(flst_realres(1:flst_reallength(flst_nreal),1:flst_nreal)) - 2
      nmaxres_born = maxval(flst_bornres(1:flst_bornlength(flst_nborn),1:flst_nborn)) - 2
      if (max(nmaxres_real,nmaxres_born) < 0) then
        ismaxres = 0
      else
        ismaxres = max(nmaxres_real,nmaxres_born)
      end if
      write(*,*) 'max number of resonances: ',ismaxres

c     add (-1) for overall azimuthal rotation of the event around the beam axis
      dim_integ = (flst_numfinal+1)*3 - 4 + 2 + ismaxres
      write(*,*) 'set ndiminteg to: ',dim_integ


      write(*,*) 'flst_nbornresgroup ',flst_nbornresgroup
      write(*,*) 'flst_nrealresgroup ',flst_nrealresgroup

      write(*,*) '*********   END SUMMARY  *********'
      write(*,*)




      call init_couplings
!       do i=3,nlegreal
!          if (abs(flst_real(i,1)).le.st_nlight) then
!             flst_lightpart=i
!             exit
!          endif
!       enddo

      call init_top_dec


cccccccccccccccccccccccccOpenLoops Initcccccccccccccccccccccccccccccccccc
      call set_parameter("order_ew", res_powew)

      call set_parameter("minnf_alphasrun", st_nlight)

! write stability log files
      if (powheginput('#openloops-stability')>0) then
        call set_parameter("stability_log", 2)
      end if

      call openloops_init
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc


      end

      subroutine init_processes_born
      implicit none
      include "nlegborn.h"
      include "pwhg_flst.h"
      include "pwhg_st.h"

        flst_bornlength = 6
        flst_nborn=     9

        flst_born(   1,1 ) = 1
        flst_born(   2,1 ) = -1
        flst_born(   3,1 ) = 6
        flst_born(   4,1 ) = -6
        flst_born(   5,1 ) = 5
        flst_born(   6,1 ) = -5

        flst_born(   1,2 ) = 2
        flst_born(   2,2 ) = -2
        flst_born(   3,2 ) = 6
        flst_born(   4,2 ) = -6
        flst_born(   5,2 ) = 5
        flst_born(   6,2 ) = -5

        flst_born(   1,3 ) = 3
        flst_born(   2,3 ) = -3
        flst_born(   3,3 ) = 6
        flst_born(   4,3 ) = -6
        flst_born(   5,3 ) = 5
        flst_born(   6,3 ) = -5

        flst_born(   1,4 ) = 4
        flst_born(   2,4 ) = -4
        flst_born(   3,4 ) = 6
        flst_born(   4,4 ) = -6
        flst_born(   5,4 ) = 5
        flst_born(   6,4 ) = -5

        flst_born(   1,5 ) = -1
        flst_born(   2,5 ) = 1
        flst_born(   3,5 ) = 6
        flst_born(   4,5 ) = -6
        flst_born(   5,5 ) = 5
        flst_born(   6,5 ) = -5

        flst_born(   1,6 ) = -2
        flst_born(   2,6 ) = 2
        flst_born(   3,6 ) = 6
        flst_born(   4,6 ) = -6
        flst_born(   5,6 ) = 5
        flst_born(   6,6 ) = -5

        flst_born(   1,7 ) = -3
        flst_born(   2,7 ) = 3
        flst_born(   3,7 ) = 6
        flst_born(   4,7 ) = -6
        flst_born(   5,7 ) = 5
        flst_born(   6,7 ) = -5

        flst_born(   1,8 ) = -4
        flst_born(   2,8 ) = 4
        flst_born(   3,8 ) = 6
        flst_born(   4,8 ) = -6
        flst_born(   5,8 ) = 5
        flst_born(   6,8 ) = -5

        flst_born(   1,9 ) = 0
        flst_born(   2,9 ) = 0
        flst_born(   3,9 ) = 6
        flst_born(   4,9 ) = -6
        flst_born(   5,9 ) = 5
        flst_born(   6,9 ) = -5


C        flst_nborn=     1
C        flst_born(   1,1 ) = 0
C        flst_born(   2,1 ) = 0
C        flst_born(   3,1 ) = 6
C        flst_born(   4,1 ) = -6
C        flst_born(   5,1 ) = 5
C        flst_born(   6,1 ) = -5


        print*,'#(born sub-processes)=',flst_nborn

      end



      subroutine init_processes_real
      implicit none
      include "nlegborn.h"
      include "pwhg_flst.h"
      include "pwhg_st.h"

        flst_nreal=     25

        flst_real(   1,1 ) = 1
        flst_real(   2,1 ) = -1
        flst_real(   3,1 ) = 6
        flst_real(   4,1 ) = -6
        flst_real(   5,1 ) = 5
        flst_real(   6,1 ) = -5
        flst_real(   7,1 ) = 0

        flst_real(   1,2 ) = 1
        flst_real(   2,2 ) = 0
        flst_real(   3,2 ) = 6
        flst_real(   4,2 ) = -6
        flst_real(   5,2 ) = 5
        flst_real(   6,2 ) = -5
        flst_real(   7,2 ) = 1

        flst_real(   1,3 ) = 2
        flst_real(   2,3 ) = -2
        flst_real(   3,3 ) = 6
        flst_real(   4,3 ) = -6
        flst_real(   5,3 ) = 5
        flst_real(   6,3 ) = -5
        flst_real(   7,3 ) = 0

        flst_real(   1,4 ) = 2
        flst_real(   2,4 ) = 0
        flst_real(   3,4 ) = 6
        flst_real(   4,4 ) = -6
        flst_real(   5,4 ) = 5
        flst_real(   6,4 ) = -5
        flst_real(   7,4 ) = 2

        flst_real(   1,5 ) = 3
        flst_real(   2,5 ) = -3
        flst_real(   3,5 ) = 6
        flst_real(   4,5 ) = -6
        flst_real(   5,5 ) = 5
        flst_real(   6,5 ) = -5
        flst_real(   7,5 ) = 0

        flst_real(   1,6 ) = 3
        flst_real(   2,6 ) = 0
        flst_real(   3,6 ) = 6
        flst_real(   4,6 ) = -6
        flst_real(   5,6 ) = 5
        flst_real(   6,6 ) = -5
        flst_real(   7,6 ) = 3

        flst_real(   1,7 ) = 4
        flst_real(   2,7 ) = -4
        flst_real(   3,7 ) = 6
        flst_real(   4,7 ) = -6
        flst_real(   5,7 ) = 5
        flst_real(   6,7 ) = -5
        flst_real(   7,7 ) = 0

        flst_real(   1,8 ) = 4
        flst_real(   2,8 ) = 0
        flst_real(   3,8 ) = 6
        flst_real(   4,8 ) = -6
        flst_real(   5,8 ) = 5
        flst_real(   6,8 ) = -5
        flst_real(   7,8 ) = 4

        flst_real(   1,9 ) = -1
        flst_real(   2,9 ) = 1
        flst_real(   3,9 ) = 6
        flst_real(   4,9 ) = -6
        flst_real(   5,9 ) = 5
        flst_real(   6,9 ) = -5
        flst_real(   7,9 ) = 0

        flst_real(   1,10 ) = -1
        flst_real(   2,10 ) = 0
        flst_real(   3,10 ) = 6
        flst_real(   4,10 ) = -6
        flst_real(   5,10 ) = 5
        flst_real(   6,10 ) = -5
        flst_real(   7,10 ) = -1

        flst_real(   1,11 ) = -2
        flst_real(   2,11 ) = 2
        flst_real(   3,11 ) = 6
        flst_real(   4,11 ) = -6
        flst_real(   5,11 ) = 5
        flst_real(   6,11 ) = -5
        flst_real(   7,11 ) = 0

        flst_real(   1,12 ) = -2
        flst_real(   2,12 ) = 0
        flst_real(   3,12 ) = 6
        flst_real(   4,12 ) = -6
        flst_real(   5,12 ) = 5
        flst_real(   6,12 ) = -5
        flst_real(   7,12 ) = -2

        flst_real(   1,13 ) = -3
        flst_real(   2,13 ) = 3
        flst_real(   3,13 ) = 6
        flst_real(   4,13 ) = -6
        flst_real(   5,13 ) = 5
        flst_real(   6,13 ) = -5
        flst_real(   7,13 ) = 0

        flst_real(   1,14 ) = -3
        flst_real(   2,14 ) = 0
        flst_real(   3,14 ) = 6
        flst_real(   4,14 ) = -6
        flst_real(   5,14 ) = 5
        flst_real(   6,14 ) = -5
        flst_real(   7,14 ) = -3

        flst_real(   1,15 ) = -4
        flst_real(   2,15 ) = 4
        flst_real(   3,15 ) = 6
        flst_real(   4,15 ) = -6
        flst_real(   5,15 ) = 5
        flst_real(   6,15 ) = -5
        flst_real(   7,15 ) = 0

        flst_real(   1,16 ) = -4
        flst_real(   2,16 ) = 0
        flst_real(   3,16 ) = 6
        flst_real(   4,16 ) = -6
        flst_real(   5,16 ) = 5
        flst_real(   6,16 ) = -5
        flst_real(   7,16 ) = -4

        flst_real(   1,17 ) = 0
        flst_real(   2,17 ) = 1
        flst_real(   3,17 ) = 6
        flst_real(   4,17 ) = -6
        flst_real(   5,17 ) = 5
        flst_real(   6,17 ) = -5
        flst_real(   7,17 ) = 1

        flst_real(   1,18 ) = 0
        flst_real(   2,18 ) = 2
        flst_real(   3,18 ) = 6
        flst_real(   4,18 ) = -6
        flst_real(   5,18 ) = 5
        flst_real(   6,18 ) = -5
        flst_real(   7,18 ) = 2

        flst_real(   1,19 ) = 0
        flst_real(   2,19 ) = 3
        flst_real(   3,19 ) = 6
        flst_real(   4,19 ) = -6
        flst_real(   5,19 ) = 5
        flst_real(   6,19 ) = -5
        flst_real(   7,19 ) = 3

        flst_real(   1,20 ) = 0
        flst_real(   2,20 ) = 4
        flst_real(   3,20 ) = 6
        flst_real(   4,20 ) = -6
        flst_real(   5,20 ) = 5
        flst_real(   6,20 ) = -5
        flst_real(   7,20 ) = 4

        flst_real(   1,21 ) = 0
        flst_real(   2,21 ) = -1
        flst_real(   3,21 ) = 6
        flst_real(   4,21 ) = -6
        flst_real(   5,21 ) = 5
        flst_real(   6,21 ) = -5
        flst_real(   7,21 ) = -1

        flst_real(   1,22 ) = 0
        flst_real(   2,22 ) = -2
        flst_real(   3,22 ) = 6
        flst_real(   4,22 ) = -6
        flst_real(   5,22 ) = 5
        flst_real(   6,22 ) = -5
        flst_real(   7,22 ) = -2

        flst_real(   1,23 ) = 0
        flst_real(   2,23 ) = -3
        flst_real(   3,23 ) = 6
        flst_real(   4,23 ) = -6
        flst_real(   5,23 ) = 5
        flst_real(   6,23 ) = -5
        flst_real(   7,23 ) = -3

        flst_real(   1,24 ) = 0
        flst_real(   2,24 ) = -4
        flst_real(   3,24 ) = 6
        flst_real(   4,24 ) = -6
        flst_real(   5,24 ) = 5
        flst_real(   6,24 ) = -5
        flst_real(   7,24 ) = -4

        flst_real(   1,25 ) = 0
        flst_real(   2,25 ) = 0
        flst_real(   3,25 ) = 6
        flst_real(   4,25 ) = -6
        flst_real(   5,25 ) = 5
        flst_real(   6,25 ) = -5
        flst_real(   7,25 ) = 0

      print*,'#(real sub-processes)=',flst_nreal
c      stop

      return
      end


      subroutine init_top_dec
      implicit none
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      include 'pwhg_rad.h'
      include 'LesHouches.h'
      integer itdec
      integer iwp1,iwp2,iwm1,iwm2
      real * 8 mdecwp1,mdecwp2,mdecwm1,mdecwm2,totbr
      real * 8 powheginput
      external powheginput

         itdec=powheginput('#topdecaymode')
         if(itdec.lt.0) itdec=0

         if(itdec.ne.0) then
            lprup(1)=300000+itdec

c first call to pickwdecay, to initialize and get back the branching fraction
            call  pickwdecays(iwp1,mdecwp1,iwp2,mdecwp2,
     #                 iwm1,mdecwm1,iwm2,mdecwm2,totbr)
            rad_branching=totbr
            print *," POWHEG: branching ratio ",totbr
         endif

      end

