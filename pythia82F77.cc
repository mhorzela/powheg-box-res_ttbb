// main01.cc is a part of the PYTHIA event generator.
// Copyright (C) 2012 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL version 2, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// This is a simple test program. It fits on one slide in a talk. 
// It studies the charged multiplicity distribution at the LHC.

#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/LHAFortran.h"
#include "Pythia8Plugins/PowhegHooks.h"
#include <sstream>
#include "Pythia8Plugins/HepMC2.h"
#include "powheginput_cpp.h"

using namespace Pythia8; 

class myLHAupFortran : public LHAupFortran {

protected:
  // User-written routine that does the intialization and fills heprup.
  virtual bool fillHepRup() {return true;}

  // User-written routine that does the event generation and fills hepeup.
  virtual bool fillHepEup() {return true;}

};

Pythia pythia;
myLHAupFortran* LHAinstance=new myLHAupFortran();
PowhegHooks* MyPowhegHooks=new PowhegHooks();
// Interface for conversion from Pythia8::Event to HepMC event.
HepMC::Pythia8ToHepMC ToHepMC;


extern "C" {
  // F77 interface to pythia8
  void pythia_option0_(char *string) {
    pythia.readString(string);
  }

  void pythia_init_() {

    // disable the listing of the changed setting (too long)
//    pythia.readString("Init:showChangedParticleData = off");

    if (powheginput("#QEDShower") != 1) {;
      // disable QED emissions
      pythia.readString("SpaceShower:QEDshowerByQ = off"); // off quarks in ISR
      pythia.readString("SpaceShower:QEDshowerByL = off"); // off leptons in ISR
      pythia.readString("TimeShower:QEDshowerByQ = off"); // off quarks in FSR
      pythia.readString("TimeShower:QEDshowerByL = off"); // off leptons in FSR
      pythia.readString("TimeShower:QEDshowerByOther = off"); // off charged resonances (including top) in FSR
      pythia.readString("TimeShower:QEDshowerByGamma = off"); // photon splittings into quarks in FSR
    }

    // tune -- set in main-PYTHIA8.f
//    pythia.readString("Tune:pp = 14"); // Monash2013 tune
//    pythia.readString("Tune:pp = 21"); // ATLAS A14 tune NNPDF2.3LO

    // twiki settings   
    if (powheginput("#stableTop") == 0) {
      pythia.readString("24:0:onMode = 0");
      pythia.readString("24:1:onMode = 0");
      pythia.readString("24:2:onMode = 0");
      pythia.readString("24:3:onMode = 0");
      pythia.readString("24:4:onMode = 0");
      pythia.readString("24:5:onMode = 0");
      if (powheginput("#diffLepFamDec") != 0) {
        pythia.readString("24:6:onMode = 2");
        pythia.readString("24:7:onMode = 3");
      }
      else {
        pythia.readString("24:6:onMode = 1");
        pythia.readString("24:7:onMode = 1");
      }
      pythia.readString("24:8:onMode = 0");
    }
    else {
      pythia.readString("6:mayDecay = off"); // top-decay
      pythia.readString("-6:mayDecay = off"); // top-decay
    }
    pythia.readString("6:m0 = 172.5"); // top mass just to check whether there is any dependence on it
    pythia.readString("-6:m0 = 172.5"); // top mass just to check whether there is any dependence on it
    pythia.readString("5:m0 = 4.75"); // bottom mass just to check whether there is any dependence on it
    pythia.readString("-5:m0 = 4.75"); // bottom mass just to check whether there is any dependence on it
//  An example on how to modify branching ratios
//    pythia.readString("24:1:meMode = 100");
//    pythia.readString("24:1:bRatio = 0.5");
    if (powheginput("#YRShowerSettings") == 1) {;

      pythia.readString("SpaceShower:alphaSuseCMW = off");
      pythia.readString("TimeShower:alphaSuseCMW = off");
  
      pythia.readString("SpaceShower:alphaSvalue = 0.118");
      pythia.readString("TimeShower:alphaSvalue = 0.118");
  
      pythia.readString("SpaceShower:renormMultFac = 1.0"); // (x_ISR)
      pythia.readString("TimeShower:renormMultFac = 1.0"); // (x_FSR)
  
      pythia.readString("SpaceShower:factorMultFac = 1.0"); // (x_ISR)
      pythia.readString("TimeShower:factorMultFac = 1.0"); // (x_FSR)
  
      pythia.readString("SpaceShower:alphaSorder = 2");
      pythia.readString("TimeShower:alphaSorder = 2");
    }

    // bb splitting settings
//    pythia.readString("SpaceShower:weightGluonToQuark = 2");
//    pythia.readString("TimeShower:weightGluonToQuark = 2");


  
    // other settings
    pythia.readString("Beams:frameType = 5");
    pythia.setUserHooksPtr(MyPowhegHooks);
    //PowhegHooks related settings 
    pythia.readString("POWHEG:nFinal = 4"); // DEF 2
    if (powheginput("#powheghooks") == 0 || powheginput("#YRShowerSettings") == 1) {
      if (powheginput("#powheghooks") == 1) cout << "powheghooks overriden to 0 because YRShowerSettigns are in action" << endl;
      pythia.readString("SpaceShower:pTmaxMatch = 1");
      pythia.readString("TimeShower:pTmaxMatch = 1");
      pythia.readString("POWHEG:veto = 0"); // DEF 0 -- set in mainPYTHIA.f
    } else {
      pythia.readString("SpaceShower:pTmaxMatch = 2");
      pythia.readString("TimeShower:pTmaxMatch = 2");
      pythia.readString("POWHEG:veto = 1"); // DEF 0 -- set in mainPYTHIA.f
    }
    pythia.readString("POWHEG:vetoCount = 3"); // DEF 3
    pythia.readString("POWHEG:pThard = 0"); // DEF 0
    pythia.readString("POWHEG:pTemt = 0"); // DEF 0
    pythia.readString("POWHEG:emitted = 0"); // DEF 0
    pythia.readString("POWHEG:pTdef = 0"); // DEF 0
    pythia.readString("POWHEG:MPIveto = 0"); // DEF 0
    pythia.readString("POWHEG:QEDveto = 0"); // DEF 0
      
    pythia.setLHAupPtr(LHAinstance); 
    LHAinstance->setInit();  
    pythia.init();
  }

  void pythia_next_(int & iret){
  // Begin event loop. Generate event. Skip if error. List first one.
    iret = pythia.next();
  }

  void pythia_to_hepevt_(const int &nmxhep, int & nhep, int * isthep,
       int * idhep,
       int  (*jmohep)[2], int (*jdahep)[2],
       double (*phep)[5], double (*vhep)[4]) {
    nhep = pythia.event.size();
    if(nhep>nmxhep) {cout << "too many particles!" ; exit(-1); }
    for (int i = 0; i < pythia.event.size(); ++i) {
      //WWarn
      *(isthep+i) = pythia.event[i].statusHepMC();
      *(idhep+i) = pythia.event[i].id();
      // All pointers should be increased by 1, since here we follow
      // the c/c++ convention of indeces from 0 to n-1, but i fortran
      // they are from 1 to n.
      (*(jmohep+i))[0] = pythia.event[i].mother1() + 1;
      (*(jmohep+i))[1] = pythia.event[i].mother2() + 1;
      (*(jdahep+i))[0] = pythia.event[i].daughter1() + 1;
      (*(jdahep+i))[1] = pythia.event[i].daughter2() + 1;
      (*(phep+i))[0] = pythia.event[i].px();
      (*(phep+i))[1] = pythia.event[i].py();
      (*(phep+i))[2] = pythia.event[i].pz();
      (*(phep+i))[3] = pythia.event[i].e();
      (*(phep+i))[4] = pythia.event[i].m();
    }
    // override mother of very first event, set to 0
    *(jmohep)[0] = 0 ;
    *(jmohep)[1] = 0 ;
  }

  HepMC::GenEvent * PythiaToHepMC(){
    // Construct new empty HepMC event and fill it.
    // Units will be as chosen for HepMC build; but can be changed
    // by arguments, e.g. GenEvt( HepMC::Units::GEV, HepMC::Units::MM)
    HepMC::GenEvent* hepmcevt = new HepMC::GenEvent();
    ToHepMC.fill_next_event( pythia, hepmcevt );
    return hepmcevt;
  }

}
