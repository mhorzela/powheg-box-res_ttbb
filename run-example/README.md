# run-example

The `powheg.input` card is suitable for `manyseeds 1` run on ~100 cores and three iterations in stage 1. In other words the following need to be run separately :

* `parallelstage 1`, `xgriditeration 1` 
* `parallelstage 1`, `xgriditeration 2` 
* `parallelstage 1`, `xgriditeration 3` 
* `parallelstage 2` 
* `parallelstage 3`, on a single core to produce `*fullgrid*` files (if not all the jobs are run on the same machine)
* `parallelstage 3`
* `parallelstage 4`

Each item should be ran on ~100 cores (with the exception of `parallelstage 3`, unless all the jobs are run a single node). Each item has to finish first before the next is launched. The full output from all the cores of a given item should be made available to each core in the next item.

Note that `pwhg_main` also accepts command line arguments overriding the settings in the `powheg.input` card. For example, to override `parallelstage 1` to `parallelstage 3`, run with `parallelstage 1` in `powheg.input` and run with:

```console
pwhg_main parallelstage=3
```
