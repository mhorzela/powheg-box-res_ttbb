      subroutine born_phsp(xborn)
      implicit none
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      include 'pwhg_kn.h'
      include 'pwhg_math.h'
      include 'PhysPars.h'
      real * 8 xborn(*)
      integer mu,j
      logical ini
      data ini/.true./
      save ini
      logical check
      parameter(check=.false.)
      real * 8 compmass
      external compmass

c generic phsp
      integer iborn
      integer flav(6),res(6)
      data flav/ 0,  0,  6, -6, 5, -5/
      data res / 0,  0,  0,  0, 0,  0/
      real * 8 p(0:3,6),cmp(0:3,6)

      if(ini) then
c     set initial- and final-state masses for ttbb Born and real
        kn_masses(1)=0
        kn_masses(2)=0
        kn_masses(3)=ph_tmass
        kn_masses(4)=ph_tmass
        kn_masses(5)=ph_bmass
        kn_masses(6)=ph_bmass
        kn_masses(nlegreal)=0
        ini=.false.
      endif 

      call genphasespace(xborn,nlegborn,flav,res,kn_beams,kn_jacborn,
     &                   kn_xb1,kn_xb2,kn_sborn,cmp,p)

      kn_cmpborn(:,1:2) = cmp(:,1:2)
      kn_pborn(:,1:2) = p(:,1:2)

      kn_cmpborn(:,1:6) = cmp(:,1:6)
      kn_pborn(:,1:6) = p(:,1:6)

      kn_minmass=kn_masses(3)+kn_masses(4)+kn_masses(5)+kn_masses(6)

c     make sure flst_ibornlength is set
      do iborn=1,flst_nborn
        if(flst_bornresgroup(iborn).eq.flst_ibornresgroup) exit
      enddo

      flst_ibornlength = flst_bornlength(iborn)
      flst_ireallength = flst_ibornlength + 1

c     momentum conservation check
      if (check) then
         write(*,*)
         do j=1,nlegborn
            write(*,*) 'mom CM',j,(kn_cmpborn(mu,j),mu=0,3)
         enddo
         do j=1,nlegborn
            write(*,*) 'mom ',j,(kn_pborn(mu,j),mu=0,3)
            write(*,*) 'mass ',j,compmass(kn_pborn(0,j))
         enddo

         call checkmomzero(nlegborn,kn_pborn)
      endif

      end

      function compmass(p)
      implicit none
      real * 8 p(0:3),compmass
      compmass = sqrt(abs(p(0)**2-p(1)**2-p(2)**2-p(3)**2))
      end

      subroutine born_suppression(fact)
      implicit none
      real * 8 fact
      fact=1d0
      end

      subroutine rmn_suppression(fact)
      implicit none
      real * 8 fact
      fact=1d0
      end

      subroutine regular_suppression(fact)
      implicit none
      real * 8 fact
      call rmn_suppression(fact)
      end

      subroutine global_suppression(c,fact)
      implicit none
      character * 1 c
      real * 8 fact
      fact=1d0
      end

      subroutine set_fac_ren_scales(muf,mur)
      implicit none
      include 'PhysPars.h'
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      include 'pwhg_flg.h'
      include 'pwhg_kn.h'
      real * 8 muf,mur
      real*8 ptop(0:3),ptbar(0:3),pb(0:3),pbbar(0:3),pg(0:3)
      real*8 mt_top,mt_tbar,mt_b,mt_bbar,mt_g,mt_light,mt_tops,mt_bottoms
      real *8 powheginput
      external powheginput
      real * 8 renscfact,facscfact
      integer, save :: runningscales
      logical, save :: ini = .true.

      if (ini) then

         renscfact=powheginput("#renscfact")
         facscfact=powheginput("#facscfact")

         runningscales = powheginput('#runningscales')

         write(*,*) '*************************************'
         write(*,*) 'Factorization and renormalization '
         if (runningscales == 0) then
           write(*,*) 'scales (mur, muf) set to '
           write(*,*) 'mur=muf=2*mtop'
         else if (runningscales == 1) then
           write(*,*) 'scales (mur, muf) set to '
           write(*,*) 'mur=[ mT(top) * mT(tbar) * mT(b) * mT(bbar) ]**(1/4)'
           write(*,*) 'muf=1/2*[ mT(top) + mT(tbar) + mT(b) + mT(bbar) + mT(gluon) ]'
         else if (runningscales == 2) then ! 'runningscales 1' with mu_r(f) > mu_r(f) / 2
           write(*,*) 'scales (mur, muf) set to '
           write(*,*) 'mur=1/2*[ mT(top) * mT(tbar) * mT(b) * mT(bbar) ]**(1/4)'
           write(*,*) 'muf=1/4*[ mT(top) + mT(tbar) + mT(b) + mT(bbar) + mT(gluon) ]'
         else
           write(*,*) "invalid `runningscales` option: ", runningscales
           write(*,*) "Exiting!"
           call exit(-1)
         endif
         if (renscfact .gt. 0d0)
     &        write(*,*) 'Renormalization scale rescaled by', renscfact
         if (facscfact .gt. 0d0)
     &        write(*,*) 'Factorization scale rescaled by  ', facscfact
         write(*,*) '***********************************************'
         ini=.false.

      endif

      if (runningscales>0) then

        if(flg_btildepart.eq.'r') then
c take real momenta:
            ptop(:)   = kn_preal(:,3) !t
            ptbar(:)  = kn_preal(:,4) !t~
            pb(:)     = kn_preal(:,5) !b
            pbbar(:)  = kn_preal(:,6) !b~
            pg(:)     = kn_preal(:,7) !parton 
        else
c take Born momenta:
            ptop(:)   = kn_pborn(:,3) !t
            ptbar(:)  = kn_pborn(:,4) !t~
            pb(:)     = kn_pborn(:,5) !b
            pbbar(:)  = kn_pborn(:,6) !b~
            pg(:)     = 0
        endif

        call gettransmass(ph_tmass,ptop,mt_top)
        call gettransmass(ph_tmass,ptbar,mt_tbar)
        call gettransmass(ph_bmass,pb,mt_b)
        call gettransmass(ph_bmass,pbbar,mt_bbar)
        mt_g = dsqrt(abs(pg(1)**2+pg(2)**2))

        if (runningscales == 1) then ! equivalent to the YR4 scales
          mur = (mt_top*mt_tbar*mt_b*mt_bbar)**(1d0/4d0)
          muf = 0.5*(mt_top+mt_tbar+mt_b+mt_bbar+mt_g)
        else if (runningscales == 2) then ! runningscale 1 with mu_r(f) > mu_r(f) / 2
          mur = 0.5*(mt_top*mt_tbar*mt_b*mt_bbar)**(1d0/4d0)
          muf = 0.25*(mt_top+mt_tbar+mt_b+mt_bbar+mt_g)
        endif 
      else
        muf=ph_tmass/2d0
        mur=muf
      endif

      end


      subroutine gettransmass(m,p,mt)
      implicit none
      real * 8 m,p(0:3),mt
      mt=dsqrt(abs(m**2+p(1)**2+p(2)**2))
      end
