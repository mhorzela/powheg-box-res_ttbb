      subroutine bornzerodamp(alr,r0,rc,rs,rcs,dampfac)
c given the R_alpha region (i.e. the alr) and the associated
c real contribution r (without pdf factor),
c returns in dampfac the damping factor to be applied to
c the real contribution to implement Born zero suppression
      implicit none
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      include 'pwhg_kn.h'
      include 'pwhg_flg.h'
      include 'pwhg_math.h'
      include 'PhysPars.h'
      integer alr
      real * 8 r0,rc,rs,rcs,dampfac,h,hFS,pwhg_pt2,pt2,powheginput,bornzerodampcut,dynhdampPF
      real * 8 pt(0:3),ptbar(0:3),pb(0:3),pbbar(0:3)
      real * 8 mt_t, mt_tbar, mt_b, mt_bbar, hto2
      integer lalr
      logical ini,dynhdamp
      data ini/.true./
      save ini,h,dynhdamp
      external pwhg_pt2,powheginput
      if(ini) then
         dynhdamp=.not.powheginput("#dynhdamp").eq.0
         dynhdampPF=powheginput("#dynhdampPF")
         if (dynhdampPF<0) dynhdampPF = 0.5d0
         h=powheginput("#hdamp")
         if(h.lt.0) then
            h=powheginput("#hfact")
            if(h.gt.0) then
               write(*,*)'***************************************'
               write(*,*)' Warning: hfact is here for backward'
               write(*,*)' compatibility with older inplementations'
               write(*,*)' New implementations will use hdamp and'
               write(*,*)' bornzerodamp independently.'
               write(*,*)'***************************************'
            endif
         endif
         if(dynhdamp) then
            write(*,*)'***************************************'
            write(*,*)' Using a damping factor h**2/(pt2+h**2)'
            write(*,*)' to separate real contributions between'
            write(*,*)' Sudakov and remnants with dynamic '
            write(*,*)' h=sqrt(1/2)*(E[t]+E[t~]), with '
            write(*,*)' E[x]=sqrt(m[x]**2+pt[x]**2)'
            write(*,*)'***************************************'
         else if(h.gt.0) then
            write(*,*)'***************************************'
            write(*,*)' Using a damping factor h^2/(pt2+h^2)'
            write(*,*)' to separate real contributions between'
            write(*,*)' Sudakov and remnants h=',h,' GeV,     ' 
            write(*,*)'***************************************'
         endif
         bornzerodampcut=powheginput('#bornzerodampcut')
         if (bornzerodampcut.lt.0) bornzerodampcut = 2d0
         ini=.false.
      endif

c set the dampfac according to three options
c 1.) dynamic hdamp
      if (dynhdamp) then
c take Born momenta (using real kinematics doesn't make much sense here)
         pt(:)     = kn_pborn(:,3) !t
         ptbar(:)  = kn_pborn(:,4) !t~
         pb(:)     = kn_pborn(:,5) !b
         pbbar(:)  = kn_pborn(:,6) !b~
         call gettransmass(ph_tmass,pt,mt_t)
         call gettransmass(ph_tmass,ptbar,mt_tbar)
         call gettransmass(ph_bmass,pb,mt_b)
         call gettransmass(ph_bmass,pbbar,mt_bbar)
         pt2=pwhg_pt2()
         hto2 = 0.5d0*(mt_t+mt_tbar+mt_b+mt_bbar) * dynhdampPF
         dampfac=hto2**2/(pt2+hto2**2)
      else if (h.gt.0) then
c 2.) static hdamp
         pt2=pwhg_pt2()
         dampfac=h**2/(pt2+h**2)
         if (flst_emitter(alr) > 2 .and. hFS > 0 .and. hFS/= h ) then
            dampfac=hFS**2/(pt2+hFS**2)
         endif
      else 
c 3.) no hdamp
         dampfac=1
      endif

      if(flg_bornzerodamp.and.r0.gt.bornzerodampcut*abs(rc+rs-rcs)) then
         dampfac=0
      endif
      end
