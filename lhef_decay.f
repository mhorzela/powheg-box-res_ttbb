      program leshouchesdecay
      use openloops
      implicit none
      include 'LesHouches.h'
      include 'pwhg_pdf.h'
      include "pwhg_st.h"
      include "pwhg_flg.h"
      include "pwhg_rnd.h"
      integer maxevin
      integer j,nev
      character * 5 scheme
      character * 512 fout
      integer iun, iorder, iret, oun
      real * 8 powheginput
      external powheginput
      integer lprefix
      character * 20 pwgprefix
      common/cpwgprefix/pwgprefix,lprefix
      integer iarg, ios
      real * 8 tmp
      character * 100 arg

c     any number of symbol=value on the command line
c     is interpreted as overriding the powheg.input value.
c     Furthermore, the special keyword iwhichseed=<integer>
c     assigns the seed number of parallel runs, preventing
c     the program to inquire for it later.
      iarg=1
      call get_command_argument(iarg,arg)
      do while(arg /= '')
         j=index(arg,'=')
         if(j>0) then
            read(arg(j+1:),fmt=*,iostat=ios) tmp
         else
            ios = -1
         endif
         if(ios == 0) then
            call powheginputoverride(arg(1:j-1),tmp)
         else
            write(*,*) 'pwhg_main: error in command line argument ',iarg
            call exit(-1)
         endif
         iarg=iarg+1
         call get_command_argument(iarg,arg)
      enddo



      pdf_ndns1=powheginput('lhans1')
      pdf_ndns2=powheginput('lhans2')
      pdf_nparton=6

      if(pdf_ndns1.ne.pdf_ndns2) then
         st_lambda5MSB=powheginput('QCDLambda5')
      else
         call genericpdfpar(pdf_ndns1,pdf_ih1,st_lambda5MSB,
     1                      scheme,iorder,iret)
         if(iret.ne.0) then
            write(*,*) ' faulty pdf number ',pdf_ndns1
            stop
         endif
      endif
      st_alpha=0.1d0

      flg_reweight=.not.(powheginput('#storeinfo_rwgt').eq.0)
      flg_rwl=.true.

      call init_couplings
      call openloops_init
      call init_top_dec
      call set_parameter("psp_tolerance", 0.0000001)

c input LHE file
      call opencountunit(nev,iun)
      write(*,*) 'EVENTS FOUND : ',nev
      call lhefreadhdr(iun)

c output LHE file
      if (powheginput('#compress_lhe').eq.1d0) then
         flg_compress_lhe=.true.
      else
         flg_compress_lhe=.false.
      endif
      fout=pwgprefix(1:lprefix)//'events-'//rnd_cwhichseed//'-decayed.lhe'
      call pwhg_io_open_write(trim(fout),oun,
     1        flg_compress_lhe,iret)
      call lhefwritehdr(oun)

      maxevin = powheginput('#maxev')
      if (maxevin>0.and.maxevin<=nev) then
        nev = maxevin
      endif

      do j=1,nev
         call lhefreadev(iun)
         if(nup.eq.0) then
            write(*,*) ' nup = 0 skipping event'
            goto 111
         endif

         call ttbb_decay
         call lhefwritev(oun)
         if (mod(j,1000).eq.0) then
            write(*,*) "# of events processed =",j
         endif
111     continue
      enddo

      call pwhg_io_close(iun)
      call pwhg_io_close(oun)
      end
