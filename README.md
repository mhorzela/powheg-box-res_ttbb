# POWHEG-BOX-RES/ttbb

## Description
Implementation of the tt~bb~ process with massive b-quarks as described in [arXiv:1802.00426](https://arxiv.org/abs/1802.00426).

## Install
Check-out `POWHEG-BOX-RES`:
```console
svn checkout [--revision n] --username anonymous --password anonymous svn://powhegbox.mib.infn.it/trunk/POWHEG-BOX-RES
```
more details at [powhegbox.mib.infn.it](http://powhegbox.mib.infn.it/index.html#res). Revision tested with: r3604.

Enter the `POWHEG-BOX-RES` directory and clone the content of the repository into `ttbb` directory:
```console
cd POWHEG-BOX-RES
git clone ssh://git@gitlab.cern.ch:7999/tjezo/powheg-box-res_ttbb.git ttbb
```

Enter the `ttbb` directory:
```console 
cd ttbb
```

And make:
```console
make
```

A `pwhg_main` binary will be produced, recommended input card can be found in the `run-example` directory.
